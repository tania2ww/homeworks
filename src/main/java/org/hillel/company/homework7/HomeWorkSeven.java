package org.hillel.company.homework7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWorkSeven {
    private static BufferedReader reader;

    public static void main(String[] args) throws IOException {
        String[] english = {"Hello! Please answer yes or no to the questions.",
                "Do you like red colour?",
                "Have you ever been to Paris?",
                "Do you have any pet?",
                "Thanks for your answers. Bye."};
        String[] ukrainian = {"Biтаю! Будь ласка дайте відповідь так чи ні на питання.",
                "Чи подобається вам червоний колір?",
                "Ви коли-небудь були в Парижі?",
                "Чи є у вас якась домашня тварина?",
                "Дякуємо за відповіді. До побачення"};
        String[] russian = {"Привет! Пожалуйста ответьте да или нет на вопросы",
                "Вам нравится красный цвет?",
                "Вы когда-нибудь были в Париже?",
                "У вас есть какое-нибудь домашнее животное?",
                "Спасибо за ваши ответы. До свидания."};
        String[] engAnswers = {"yes", "no", "You input incorrect answer. Please repeat."};
        String[] rusAnswers = {"да", "нет", "Вы ввели неправильный ответ. Пожалуйста повторите"};
        String[] ukrAnswers = {"так", "нi", "Ви ввели неправильну відповідь. Будь ласка повторіть"};


        reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println(" Please choose a language: ENG - english, UKR - український , RUS - русский ");
            String userChoice = getInputData(reader);
            if (userChoice.equalsIgnoreCase("ENG")) {
                printQuestionsForUser(english, engAnswers);
                break;
            }
            if (userChoice.equalsIgnoreCase("UKR")) {
                printQuestionsForUser(ukrainian, ukrAnswers);
                break;
            }
            if (userChoice.equalsIgnoreCase("RUS")) {
                printQuestionsForUser(russian, rusAnswers);
                break;
            } else {
                System.out.println("Input data is incorrect. Please try again.");
            }
        }
        reader.close();
    }

    private static String getInputData(BufferedReader reader) {
        try {
            return reader.readLine();
        } catch (Exception e) {
            return "";
        }
    }

    private static void printQuestionsForUser(String[] array, String[] answers) {
        System.out.println(array[0]);
        System.out.println(array[1]);
        for (int i = 2; i < array.length; i++) {
             getAnswer(answers);
            System.out.println(array[i]);
        }
    }

    private static void getAnswer(String[] answers) {
        while (true) {
            String answer = getInputData(reader);
            if (!answer.equalsIgnoreCase(answers[0]) && !answer.equalsIgnoreCase(answers[1])) {
                System.out.println(answers[2]);
            } else {
                break;
            }
        }
    }
}
