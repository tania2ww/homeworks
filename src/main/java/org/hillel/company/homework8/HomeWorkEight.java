package org.hillel.company.homework8;

public class HomeWorkEight {
    public static void main(String[] args) {
        char[][] array = new char[10][10];
        fillWithPoints(array);
        printArray(array);

        fillOverDiagonal(array);
        printArray(array);

        fillUnderDiagonal(array);
        printArray(array);

        fillDiagonal(array);
        printArray(array);
    }

    private static void fillWithPoints(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = '.';
            }
        }
    }


    private static void fillOverDiagonal(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i < j) {
                    array[i][j] = '0';
                }
            }
        }
    }

    private static void fillUnderDiagonal(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i > j) {
                    array[i][j] = '1';
                }
            }
        }
    }

    private static void fillDiagonal(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    array[i][j] = '-';
                }
            }
        }
    }

    private static void printArray(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
