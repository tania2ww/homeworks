package org.hillel.company.homework9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWorkNine {
    public static void main(String[] args) throws IOException {

        Book[] books = new Book[12];
        books[0] = new Book("A Promised Land", "B.Obama", " 2-266-11156-6", new Year(2020));
        books[1] = new Book("Ready Player Two ", " E.Cline", " 2-266-11157-6", new Year(2020));
        books[2] = new Book("Reality and Other Stories", "J.Lanchester", " 2-266-11156-6",
                new Year(2020));
        books[3] = new Book("Disappearing Earth", "J.Phillips", " 2-266-11158-6", new Year(2019));
        books[4] = new Book("The Great Gatsby", "S.Fitzgerald", " 2-266-11159-6", new Year(1925));
        books[5] = new Book("An American Tragedy", " T.Dreiser", " 2-266-11150-6", new Year(1925));
        books[6] = new Book("White Noise", "D.Delillo", " 2-266-11153-6", new Year(1985));
        books[7] = new Book("Blood Meridian", " C.McCarthy", " 2-266-11154-6", new Year(1985));
        books[8] = new Book("A Tree Grows in Brooklyn", "B.Smith", " 2-266-11156-9",
                new Year(1943));
        books[9] = new Book("Heart of Darkness", "J.Conrad", " 2-266-11156-4", new Year(1902));
        books[10] = new Book("The Hound of the Baskervilles", "A.C.Doyle", " 2-266-11156-3",
                new Year(1902));
        books[11] = new Book("The Dutch House", "A.Pathett", " 2-266-11156-2", new Year(2019));

        for (Book book : books) {
            System.out.println("Book " + book.getTitle() + " was written by " + book.getAuthor() +
                    ", has isbn =" + book.getIsbn() + ", was published in " + book.getYear());
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Hello");
        while (true) {
            System.out.println(" Please input: 1 - to find out what books were published  or 2 - for exit ");
            int userChoice = getInputData(reader);
            if (userChoice < 0 || userChoice > 2) {
                System.out.println(" You input wrong number. Please repeat.");
                continue;
            }
            if (userChoice == 1) {
                int inputIssueYear = getInputYear(reader);
                printInformationForUser(inputIssueYear, books);
            } else {
                System.out.println("Thanks. Bye.");
                break;
            }
        }
        reader.close();
    }

    private static int getInputData(BufferedReader reader) {
        try {
            return Integer.parseInt(reader.readLine());
        } catch (Exception e) {
            return -1;
        }
    }

    private static int getInputYear(BufferedReader reader) {
        while (true) {
            System.out.println("Please input year ");
            int userData = getInputData(reader);
            if (userData <= 0 || userData > 2021) {
                System.out.println("You input incorrect year. Try again.");
            } else {
                return userData;
            }
        }
    }

    private static void printInformationForUser(int inputYear, Book[] books) {
        boolean wasBookFound = false;
        for (Book book : books) {
            if (inputYear == book.getYear().getValue()) {
                wasBookFound = true;
                System.out.println("In " + inputYear + " year was published book: " + book.getTitle() +
                        ", which was written by " + book.getAuthor() +
                        ", isbn =" + book.getIsbn());
            }
        }
        if (!wasBookFound) {
            System.out.println("There isn't any book in our list which was published in this year");
        }
    }
}
