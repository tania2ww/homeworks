package org.hillel.company.homework10;

public class Author {
    private String lastName;
    private String country;
    private int dateOfBirth;

    public Author(String lastName, String country, int dateOfBirth) {
        this.lastName = lastName;
        this.country = country;
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        country = country;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Author{" +
                "lastName='" + lastName + '\'' +
                ", Country='" + country + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
