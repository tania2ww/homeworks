package org.hillel.company.homework10;

public class ClassicBook extends Book {
    public ClassicBook(String title, Author author, String isbn, Year year) {
        super(title, author, isbn, year);
    }

    public static void main(String[] args) {
        int[] arrayInt = {1, 2, 3};
        String[] arrayString = {"One", "Two"};
        printArray(arrayInt);
        printArray(arrayString);

    }

    public static void printArray(int[] array) {
        for (int element : array) {
            System.out.println(element);
        }
    }

    public static void printArray(String[] array) {
        for (String element : array) {
            System.out.println(element);
        }
    }
}
