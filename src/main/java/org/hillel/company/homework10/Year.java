package org.hillel.company.homework10;

public class Year {
    private final int value;

    public Year(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return
                "year = " + value;
    }
}
