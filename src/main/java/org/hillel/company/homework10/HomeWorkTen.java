package org.hillel.company.homework10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWorkTen {
    public static void main(String[] args) throws IOException {
        Book[] books = new Book[12];
        books[0] = new Book("A Promised Land", new Author("Obama", "USA", 1961),
                " 2-266-11156-6", new Year(2020));
        books[1] = new Book("Ready Player Two ", new Author("Cline", "USA", 1895),
                " 2-266-11157-6", new Year(2020));
        books[2] = new Book("Reality and Other Stories",
                new Author("Lanchester", "Germany", 1962),
                " 2-266-11156-6", new Year(2020));
        books[3] = new Book("Disappearing Earth", new Author("Phillips", "USA", 1944),
                " 2-266-11158-6", new Year(2019));
        books[4] = new Book("The Great Gatsby", new Author("Fitzgerald", "USA", 1920),
                " 2-266-11159-6", new Year(1925));
        books[5] = new Book("An American Tragedy", new Author(" Dreiser", "USA", 1871),
                " 2-266-11150-6", new Year(1925));
        books[6] = new Book("White Noise", new Author("Delillo", "USA", 1936),
                " 2-266-11153-6", new Year(1985));
        books[7] = new Book("Blood Meridian", new Author("McCarthy", "USA", 1959),
                " 2-266-11154-6", new Year(1985));
        books[8] = new Book("A Tree Grows in Brooklyn", new Author("Smith", "USA", 1949),
                " 2-266-11156-9", new Year(1943));
        books[9] = new Book("Heart of Darkness", new Author("Conrad", "Russia", 1924),
                " 2-266-11156-4", new Year(1902));
        books[10] = new Book("The Hound of the Baskervilles",
                new Author("Doyle", "Scotland", 1859),
                " 2-266-11156-3", new Year(1902));
        books[11] = new Book("The Dutch House", new Author("Pathett", "USA", 1963),
                " 2-266-11156-2", new Year(2019));

        for (Book book : books) {
            System.out.println(book);
        }

        HomeWorkTen homeWorkTen = new HomeWorkTen();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Hello");
        while (true) {
            System.out.println(" Please input: 1 - to find out which books were published in this year," +
                    " 2 - to find out which books were written by this author or 3 - for exit ");
            int userChoice = homeWorkTen.getInputNumber(reader);
            if (userChoice == 1) {
                System.out.println("Please input year");
                String inputYear = homeWorkTen.getInputData(reader);
                homeWorkTen.printInfo(inputYear, books);
            } else if (userChoice == 2) {
                System.out.println("Please input author's last name");
                String inputAuthor = homeWorkTen.getInputData(reader);
                homeWorkTen.printInfo(inputAuthor, books);
            } else if (userChoice == 3) {
                System.out.println("Thanks. Bye.");
                break;
            } else {
                System.out.println(" You input wrong number. Please repeat.");
            }
        }
        reader.close();
    }


    private int getInputNumber(BufferedReader reader) {
        try {
            return Integer.parseInt(reader.readLine());
        } catch (NumberFormatException | IOException e) {
            return -1;
        }
    }

    private String getInputData(BufferedReader reader) {
        try {
            return reader.readLine();
        } catch (IOException e) {
            return "";
        }
    }

    private void printInfo(String inputData, Book[] books) {
        boolean wasBookFound = false;
        for (Book book : books) {
            boolean isBookFitsByYear = inputData.equalsIgnoreCase(String.valueOf(book.getYear().getValue()));
            boolean isBookFitsByAuthor = inputData.equalsIgnoreCase(book.getAuthor().getLastName());
            if (isBookFitsByYear || isBookFitsByAuthor) {
                wasBookFound = true;
                System.out.println(book);
            }
        }
        if (!wasBookFound) {
            System.out.println("Any book wasn't found");
        }
    }
}

