package org.hillel.company.homework11;

public class HomeWorkEleven {
    public static void main(String[] args) {
        Elf elfOne = new Elf(100, 0, null, "elf one",
                190, 100, "male", 1000);
        Elf elfTwo = new Elf(250, 0, null, "elf two", 205,
                35, "male", 900);
        ElfArcher elfArcherOne = new ElfArcher(150, 300, "bow",
                "elf archer one", 210, 75, "male", 1500);
        ElfArcher elfArcherTwo = new ElfArcher(300, 400, "bow",
                "elf archer two", 170, 83, "male", 1200);
        ElfArcher elfArcherThree = new ElfArcher(350, 120, "bow", "elf archer three",
                185, 57, "male", 700);
        ElfSwordsman elfSwordsmanOne = new ElfSwordsman(500, 300, "sword", "elf swordsman one",
                198, 77, "male", 1060);
        ElfSwordsman elfSwordsmanTwo = new ElfSwordsman(350, 50, "sword",
                "elf swordsman two", 200, 65, "male", 2000);
        ElfSwordsman elfSwordsmanThree = new ElfSwordsman(100, 65, "sword",
                "elf swordsman three", 186, 43, "male", 1300);
        ElfSwordsman elfSwordsmanFour = new ElfSwordsman(90, 200, "sword",
                "elf swordsman four", 215, 58, "male", 1500);
        ElfSwordsman elfSwordsmanFive = new ElfSwordsman(350, 800, "sword",
                "elf swordsman five", 196, 95, "male", 3000);
        Dwarf dwarfOne = new Dwarf(300, 0, null, "dwarf one", 90,
                120, "male", 220);
        Dwarf dwarfTwo = new Dwarf(300, 0, null, "dwarf two", 80,
                80, "male", 400);

        new HomeWorkEleven().greetings(elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, elfSwordsmanOne,
                elfSwordsmanTwo, elfSwordsmanThree, elfSwordsmanFour, elfSwordsmanFive);
        new HomeWorkEleven().greetings(dwarfOne, dwarfTwo);
        System.out.println();
        new HomeWorkEleven().toAttack(elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, elfSwordsmanOne,
                elfSwordsmanTwo, elfSwordsmanThree, elfSwordsmanFour, elfSwordsmanFive, dwarfOne, dwarfTwo);
        System.out.println();
        new HomeWorkEleven().toDefence(elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, elfSwordsmanOne,
                elfSwordsmanTwo, elfSwordsmanThree, elfSwordsmanFour, elfSwordsmanFive, dwarfOne, dwarfTwo);
    }

    private void greetings(Elf... elves) {
        for (Elf elf : elves) {
            elf.greetings();
            System.out.println();
        }
    }

    private void greetings(Dwarf... dwarves) {
        for (Dwarf dwarf : dwarves) {
            dwarf.greetings();
            System.out.println();
        }
    }

    private void toAttack(Warrior... warriors) {
        for (Warrior warrior : warriors) {
            warrior.attack();
        }
    }

    private void toDefence(Warrior... warriors) {
        for (Warrior warrior : warriors) {
            warrior.defence();
        }
    }
}
