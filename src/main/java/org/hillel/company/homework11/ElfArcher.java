package org.hillel.company.homework11;

public class ElfArcher extends Elf {

    public ElfArcher(int powerOfDefence, int powerOfWeaponHit, String weapon, String name, int height, int age, String sex, int power) {
        super(powerOfDefence, powerOfWeaponHit, weapon, name, height, age, sex, power);
    }
}
