package org.hillel.company.homework11;

public class ElfSwordsman extends Elf {

    public ElfSwordsman(int powerOfDefence, int powerOfWeaponHit, String weapon, String name, int height, int age,
                        String sex, int power) {
        super(powerOfDefence, powerOfWeaponHit, weapon, name, height, age, sex, power);
    }
}
