package org.hillel.company.homework11;

public abstract class Warrior {
    private int powerOfDefence;
    private int powerOfWeaponHit;
    private String weapon;

    public Warrior(int powerOfDefence, int powerOfWeaponHit, String weapon) {
        this.powerOfDefence = powerOfDefence;
        this.powerOfWeaponHit = powerOfWeaponHit;
        this.weapon = weapon;
    }

    public int getPowerOfDefence() {
        return powerOfDefence;
    }

    public void setPowerOfDefence(int powerOfDefence) {
        this.powerOfDefence = powerOfDefence;
    }

    public int getPowerOfWeaponHit() {
        return powerOfWeaponHit;
    }

    public void setPowerOfWeaponHit(int powerOfWeaponHit) {
        this.powerOfWeaponHit = powerOfWeaponHit;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String sayAboutWeapon() {
        if (weapon == null) {
            return "I don't have any weapon";
        } else {
            return "I have a " + weapon;
        }
    }

    public abstract void attack();

    public abstract void defence();
}
