package org.hillel.company.homework12;

public class NoWeapon implements Weapon {

    @Override
    public String sayAboutWeapon() {
        return "I don't have any weapon";
    }
}
