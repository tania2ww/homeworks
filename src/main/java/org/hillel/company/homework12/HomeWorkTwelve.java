package org.hillel.company.homework12;

public class HomeWorkTwelve {
    public static void main(String[] args) {
        Elf elfOne = new Elf(100, 0, new NoWeapon(), "elf one",
                190, 100, "male", 1000);
        Elf elfTwo = new Elf(250, 0, new NoWeapon(), "elf two", 205,
                35, "male", 900);
        ElfArcher elfArcherOne = new ElfArcher(150, 300, new Bow(),
                "elf archer one", 210, 75, "male", 1500);
        ElfArcher elfArcherTwo = new ElfArcher(300, 400, new Bow(),
                "elf archer two", 170, 83, "male", 1200);
        ElfArcher elfArcherThree = new ElfArcher(350, 120, new Bow(),
                "elf archer three", 185, 57, "male", 700);
        ElfSwordsman elfSwordsmanOne = new ElfSwordsman(500, 300, new Sword(),
                "elf swordsman one", 198, 77, "male", 1060);
        ElfSwordsman elfSwordsmanTwo = new ElfSwordsman(350, 50, new Sword(),
                "elf swordsman two", 200, 65, "male", 2000);
        ElfSwordsman elfSwordsmanThree = new ElfSwordsman(100, 65, new Sword(),
                "elf swordsman three", 186, 43, "male", 1300);
        ElfSwordsman elfSwordsmanFour = new ElfSwordsman(90, 200, new Sword(),
                "elf swordsman four", 215, 58, "male", 1500);
        ElfSwordsman elfSwordsmanFive = new ElfSwordsman(350, 800, new Sword(),
                "elf swordsman five", 196, 95, "male", 3000);
        Dwarf dwarfOne = new Dwarf(300, 0, new NoWeapon(), "dwarf one", 90,
                120, "male", 220);
        Dwarf dwarfTwo = new Dwarf(300, 0, new NoWeapon(), "dwarf two", 80,
                80, "male", 400);


        Warrior[] warriors = {elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, elfSwordsmanOne,
                elfSwordsmanTwo, elfSwordsmanThree, elfSwordsmanFour, elfSwordsmanFive, dwarfOne, dwarfTwo};

        HomeWorkTwelve homeWorkTwelve = new HomeWorkTwelve();
        homeWorkTwelve.toAttack(warriors);
        System.out.println();
        homeWorkTwelve.toJump(warriors);
        System.out.println();
        homeWorkTwelve.toRunFast(warriors);
    }

    private void greetings(Elf... elves) {
        for (Elf elf : elves) {
            elf.greetings();
            System.out.println();
        }
    }

    private void greetings(Dwarf... dwarves) {
        for (Dwarf dwarf : dwarves) {
            dwarf.greetings();
            System.out.println();
        }
    }

    private void toAttack(Warrior[] warriors) {
        for (Warrior warrior : warriors) {
            if (warrior instanceof FastRunnable && warrior instanceof Jumpable) {
                warrior.attack();
            }
        }
    }

    private void toRunFast(Warrior[] warriors) {
        for (Warrior warrior : warriors) {
            if (warrior instanceof FastRunnable) {
                ((FastRunnable) warrior).fastRun();
            }
        }
    }

    private void toJump(Warrior[] warriors) {
        for (Warrior warrior : warriors) {
            if (warrior instanceof Jumpable) {
                ((Jumpable) warrior).jump();
            }
        }
    }

    private void toDefence(Warrior... warriors) {
        for (Warrior warrior : warriors) {
            warrior.defence();
        }
    }
}
