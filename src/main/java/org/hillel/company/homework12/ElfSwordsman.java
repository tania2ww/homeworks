package org.hillel.company.homework12;

public class ElfSwordsman extends Elf implements Jumpable {
    public ElfSwordsman(int powerOfDefence, int powerOfWeaponHit, Weapon weapon, String name, int height, int age,
                        String sex, int power) {
        super(powerOfDefence, powerOfWeaponHit, weapon, name, height, age, sex, power);
    }

    @Override
    public void jump() {
        System.out.println("I'm an " + getName() + ", I can jump");
    }
}
