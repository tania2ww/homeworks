package org.hillel.company.homework12;

public class Sword implements Weapon {
    @Override
    public String sayAboutWeapon() {
        return "I have a sword";
    }
}
