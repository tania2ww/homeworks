package org.hillel.company.homework12;

public class Dwarf extends Warrior {
    private String name;
    private int height;
    private int age;
    private String sex;
    private int power;

    public Dwarf(int powerOfDefence, int powerOfWeaponHit, Weapon weapon, String name, int height, int age, String sex,
                 int power) {
        super(powerOfDefence, powerOfWeaponHit, weapon);
        this.name = name;
        this.height = height;
        this.age = age;
        this.sex = sex;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }


    public void greetings() {
        System.out.println("Hello, I'm a " + name + ", my parameters are: height " + height + ", age " + age +
                ", sex " + sex + ", power " + power + ", " + sayAboutWeapon());
    }

    @Override
    public void attack() {
        System.out.println("I'm a " + name + ", I'm attacking , my power is " + power + ", the amount of damage is" +
                (power + getPowerOfWeaponHit()));
    }

    @Override
    public void defence() {
        System.out.println("I'm an " + name + ", I'm defencing, my power of defence is " + getPowerOfDefence());
    }
}
