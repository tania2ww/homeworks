package org.hillel.company.homework12;

public class Bow implements Weapon {

    @Override
    public String sayAboutWeapon() {
        return "I have a bow";
    }
}
