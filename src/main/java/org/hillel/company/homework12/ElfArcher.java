package org.hillel.company.homework12;

public class ElfArcher extends Elf {
    public ElfArcher(int powerOfDefence, int powerOfWeaponHit, Weapon weapon, String name, int height, int age,
                     String sex, int power) {
        super(powerOfDefence, powerOfWeaponHit, weapon, name, height, age, sex, power);
    }
}
