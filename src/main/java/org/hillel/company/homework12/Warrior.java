package org.hillel.company.homework12;

public abstract class Warrior {
    private int powerOfDefence;
    private int powerOfWeaponHit;
    private Weapon weapon;

    public Warrior(int powerOfDefence, int powerOfWeaponHit, Weapon weapon) {
        this.powerOfDefence = powerOfDefence;
        this.powerOfWeaponHit = powerOfWeaponHit;
        this.weapon = weapon;
    }

    public int getPowerOfDefence() {
        return powerOfDefence;
    }

    public void setPowerOfDefence(int powerOfDefence) {
        this.powerOfDefence = powerOfDefence;
    }

    public int getPowerOfWeaponHit() {
        return powerOfWeaponHit;
    }

    public void setPowerOfWeaponHit(int powerOfWeaponHit) {
        this.powerOfWeaponHit = powerOfWeaponHit;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public String sayAboutWeapon() {
        return weapon.sayAboutWeapon();
    }

    public abstract void attack();

    public abstract void defence();
}
