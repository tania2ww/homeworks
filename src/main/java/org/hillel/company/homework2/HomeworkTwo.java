package org.hillel.company.homework2;

public class HomeworkTwo {
    private static final double ADDITIONAL_RATE = 0.2;

    public static void main(String[] args) {
        int pricePerKm = 250;
        int distance = 1000;
        int ticketPrice = getTicketPrice(distance, pricePerKm);
        printInformation(ticketPrice, pricePerKm, distance);
        System.out.println("Ticket price with additional rate is: " +
                getTicketPriceWithAdditionalRate(ticketPrice));
    }

    private static int getTicketPrice(int distance, int pricePerKm) {
        return distance * pricePerKm;
    }

    private static void printInformation(int ticketPrice, int pricePerKm, int distance) {
        System.out.println("Ticket price is: " + ticketPrice + ", " +
                "where the price per 1km is: " + pricePerKm + " and the distance is: " + distance);
    }

    private static double getTicketPriceWithAdditionalRate(int ticketPrice) {
        return ticketPrice * ADDITIONAL_RATE;
    }
}
