package org.hillel.company.homework6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWorkSix {
    private static BufferedReader reader;

    public static void main(String[] args) throws IOException {
        String[] books = {"A Promised Land by B.Obama", "Ready Player Two by E.Cline",
                "Reality and Other Stories by J.Lanchester", "Disappearing Earth by J.Phillips",
                "The Great Gatsby by S.Fitzgerald", "An American Tragedy by T.Dreiser",
                "White Noise by D.Delillo", "Blood Meridian by C.McCarthy",
                "A Tree Grows in Brooklyn by B.Smith", "Heart of Darkness by J.Conrad",
                "The Hound of the Baskervilles by A.C.Doyle", "The Dutch House by A.Pathett"};
        Integer[] years = {1902, 1925, 1943, 1985, 2019, 2020};

        printArray(books);
        printArray(years);
        printReversedArray(books);
        printReversedArray(years);

        Integer[] yearsTwo = {2020, 2020, 2020, 2019, 1925, 1925,
                1985, 1985, 1943, 1902, 1902, 2019};
        int[] yearsThree = {5, 5, 5, 4, 1, 1, 3, 2, 0, 4};
        printTitlesWithYears(books, yearsTwo);

        reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Hello");
        while (true) {
            System.out.println(" Please input: 1 - to find out what books were published  or 2 - for exit ");
            int userChoice = getInputData(reader);
            if (userChoice < 0 || userChoice > 2) {
                System.out.println(" You input wrong number. Please repeat.");
                continue;
            }
            if (userChoice == 1) {
                int inputIssueYear = getInputYear();
                printInformationForUser(inputIssueYear, yearsTwo, books);
            } else {
                System.out.println("Thanks. Bye.");
                break;
            }
        }
        reader.close();

    }

    private static void printArray(Object[] array) {
        for (Object element : array) {
            System.out.println(element);
        }
    }

    private static void printReversedArray(Object[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.println(array[i]);
        }
    }

    private static void printTitlesWithYears(Object[] arrayBooks, Object[] arrayYears) {
        for (int i = 0; i < arrayBooks.length; i++) {
            System.out.println(arrayBooks[i] + " issue year is " + arrayYears[i]);
        }
    }

    private static int getInputData(BufferedReader reader) {
        try {
            return Integer.parseInt(reader.readLine());
        } catch (Exception e) {
            return -1;
        }
    }

    private static int getInputYear() {
        while (true) {
            System.out.println("Please input year ");
            int userData = getInputData(reader);
            if (userData <= 0 || userData > 2021) {
                System.out.println("You input incorrect year. Try again.");
            } else {
                return userData;
            }
        }
    }

    private static void printInformationForUser(int inputYear, Integer[] arrayYears, String[] arrayBooks) {
        boolean wasBookFound = false;
        for (int i = 0; i < arrayYears.length; i++) {
            if (inputYear == arrayYears[i]) {
                wasBookFound = true;
                System.out.println("In " + inputYear + " year was published: " + arrayBooks[i]);
            }
        }
        if (!wasBookFound) {
            System.out.println("There isn't any book in our list which was published in this year");
        }
    }
}
