package org.hillel.company.homework4;

public class HomeWorkFour {
    public static void main(String[] args) {
//        task 1
        printType(Byte.MIN_VALUE, Byte.MAX_VALUE, "byte");
        printType(Short.MIN_VALUE, Short.MAX_VALUE, "short");
        printType(Integer.MIN_VALUE, Integer.MAX_VALUE, "int");
        printType(Long.MIN_VALUE, Long.MAX_VALUE, "long");

//        task 2
        byte typeByte = 100;
        short typeShort;
        typeShort = typeByte;
        int typeInt;
        long typeLong = 1000000000;
        typeInt = (int) typeLong;

        System.out.println("Automatic type casting: short value = " + typeShort);
        System.out.println("Explicit type casting: int value = " + typeInt);

//          task 3
        byte typeByteTwo = (byte) 128;
        short typeShortTwo = (short) -72768;
        int typeIntTwo = (int) 25000078912L;
        System.out.println("Data loss: typeByteTwo = " + typeByteTwo + ", typeShortTwo = " + typeShortTwo +
                ", typeIntTwo = " + typeIntTwo);
    }

    private static void printType(long min, long max, String type) {
        System.out.println("Minimum for " + type + " type " + min);
        System.out.println(("Maximum for " + type + " type " + max));
    }
}
