package org.hillel.company.homework5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWorkFive {
    private static final int DISTANCE_TO_BERLIN = 1800;
    private static final int DISTANCE_TO_LONDON = 2800;
    private static final int DISTANCE_TO_MADRID = 4100;
    private static final Account agencyAccount = new Account(0);
    private static final Account clientAccount = new Account(48000);

    public static void main(String[] args) {
        int pricePerKm = 5;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Please enter number of tickets");
                int inputNumber = getInputData(reader);
                int numbersOfTicketsAvailableForSale = 50;
                if (inputNumber <= numbersOfTicketsAvailableForSale && inputNumber > 0) {
                    debitingFundsFromAccount(clientAccount, getTicketPrice(DISTANCE_TO_BERLIN, pricePerKm), inputNumber);
                    System.out.println("Balance of agency account = " + agencyAccount);
                    break;
                } else {
                    System.out.println("You enter incorrect data. Please repeat");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int getTicketPrice(int distance, int pricePerKm) {
        return distance * pricePerKm;
    }

    private static void debitingFundsFromAccount(Account account, int ticketPrice, int numbersOfTickets) {
        for (int i = 1; i <= numbersOfTickets; i++) {
            if (account.getBalance() >= ticketPrice) {
                account.changeBalance(-ticketPrice);
                agencyAccount.changeBalance(ticketPrice);
                System.out.println("You have bought " + i + " tickets. Your balance = " + account);
            } else {
                System.out.println("You don't have enough money.");
                break;
            }
        }
    }

    private static int getInputData(BufferedReader reader) throws IOException {
        try {
            String userData = reader.readLine();
            return Integer.parseInt(userData);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}