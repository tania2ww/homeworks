package org.hillel.company.homework5;

public class Account {
    private int balance;

    public Account(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.valueOf(balance);
    }

    public void changeBalance(int money) {
        balance += money;
    }
}
