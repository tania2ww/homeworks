package org.hillel.company.homework3;

public class HomeWorkThree {
    private static final String BERLIN = "Berlin";
    private static final int DISTANCE_TO_BERLIN = 1800;
    private static final String LONDON = "London";
    private static final int DISTANCE_TO_LONDON = 2800;
    private static final String MADRID = "Madrid";
    private static final int DISTANCE_TO_MADRID = 4100;

    public static void main(String[] args) {
        int pricePerKm = 5;

        int ticketPriceToBerlin = getTicketPrice(DISTANCE_TO_BERLIN, pricePerKm);
        int ticketPriceToLondon = getTicketPrice(DISTANCE_TO_LONDON, pricePerKm);
        int ticketPriceToMadrid = getTicketPrice(DISTANCE_TO_MADRID, pricePerKm);

        printInformation(ticketPriceToBerlin, pricePerKm, DISTANCE_TO_BERLIN, BERLIN);
        printInformation(ticketPriceToLondon, pricePerKm, DISTANCE_TO_LONDON, LONDON);
        printInformation(ticketPriceToMadrid, pricePerKm, DISTANCE_TO_MADRID, MADRID);

        String allDirection = "";
        allDirection = getsmth(BERLIN, LONDON, MADRID);
        System.out.println("All directions are: " + allDirection);
    }

    private static int getTicketPrice(int distance, int pricePerKm) {
        return distance * pricePerKm;
    }

    private static void printInformation(int ticketPrice, int pricePerKm, int distance, String city) {
        System.out.println("Ticket price to: " + city + " is " + ticketPrice + ", " +
                "where the price per 1km is: " + pricePerKm + " and the distance is: " + distance);
    }


    private static String getAllDirection(String... cities) {
        StringBuilder sb = new StringBuilder();
        for (String city : cities) {
            sb.append(sb.length() > 0 ? "," : "");
            sb.append(city);
        }
        return sb.toString();
    }

    private static String getsmth(String... cities) {
        String result = "";
        for (String city : cities) {
            result += (result.length() > 0 ? ", " : "") + city;
        }
        return result;
    }
}

