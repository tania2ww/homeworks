package org.hillel.company.homework13;

public interface Attackable {
    int attack();
}
