package org.hillel.company.homework13;

public class Dwarf extends Warrior implements Attackable, Defenceable {
    private int livePoints;

    public Dwarf(int livePoints) {
        this.livePoints = livePoints;
    }

    @Override
    public int  attack() {
        return attackPoints;
    }

    @Override
    public void defence(int attackPoints) {
        if(attackPoints <= defenceLevel){
            defenceLevel = defenceLevel - attackPoints;
            return;
        }
        int rest = attackPoints - defenceLevel;
        defenceLevel = 0;
        livePoints = Math.max(livePoints - rest, 0);

        changeHealthState(livePoints);
    }

    @Override
    public String toString() {
        return "Dwarf{" +
                ", healthState=" + healthState +
                ", livePoints=" + livePoints +
                ", defenceLevel=" + defenceLevel +
                ", attackPoints=" + attackPoints +
                '}';
    }


}
