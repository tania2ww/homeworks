package org.hillel.company.homework13;

import java.util.Random;

public abstract class Warrior {
    protected HealthState healthState = HealthState.GOOD;
    protected int defenceLevel;
    protected int attackPoints;

    public HealthState getHealthState() {
        return healthState;
    }

    public void initDefenceLevel() {
        this.defenceLevel = new Random().nextInt(100);
    }

    public void initAttackLevel() {
        this.attackPoints = new Random().nextInt(50);
    }

    protected void changeHealthState(int livePoints) {
        HealthState[] values = HealthState.values();
        for (HealthState value : values) {
            if (livePoints >= value.getMinLevel() && livePoints <= value.getMaxLevel()) {
                healthState = value;
            }
        }
    }
}
