package org.hillel.company.homework13;

public interface Defenceable {
    void defence(int attackPoints);
}
