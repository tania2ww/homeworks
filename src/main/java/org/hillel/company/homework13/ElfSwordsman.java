package org.hillel.company.homework13;

public class ElfSwordsman extends Elf {
    int countAttack = 3;
    int blockedAttack = 25;

    public ElfSwordsman(int livePoints) {
        super(livePoints);
    }

    @Override
    public int attack() {
        return attackPoints;
    }

    @Override
    public void defence(int attackPoints) {
        attackPoints = (attackPoints * (100 - blockedAttack)) / 100;
        if (countAttack > 0) {
            countAttack--;
            return;
        }
        if (attackPoints <= defenceLevel) {
            defenceLevel = defenceLevel - attackPoints;
            return;
        }
        int rest = attackPoints - defenceLevel;
        defenceLevel = 0;
        livePoints = Math.max(livePoints - rest, 0);

        changeHealthState(livePoints);
    }
}
