package org.hillel.company.homework13;

public class Elf extends Warrior implements Attackable, Defenceable {
    protected int livePoints;

    public Elf(int livePoints) {
        this.livePoints = livePoints;
    }

    @Override
    public int attack() {
        return attackPoints;
    }

    @Override
    public void defence(int attackPoints) {
        if (attackPoints <= defenceLevel) {
            defenceLevel = defenceLevel - attackPoints;
            return;
        }
        int rest = attackPoints - defenceLevel;
        defenceLevel = 0;
        livePoints = Math.max(livePoints - rest, 0);

        changeHealthState(livePoints);
    }

    @Override
    public String toString() {
        return "Elf{" +
                ", healthState=" + getHealthState() +
                ", livePoints=" + livePoints +
                ", defenceLevel=" + defenceLevel +
                ", attackPoints=" + attackPoints +
                '}';
    }


}
