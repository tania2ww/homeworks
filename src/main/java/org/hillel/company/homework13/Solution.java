package org.hillel.company.homework13;

import java.util.Arrays;

import static org.hillel.company.homework13.HealthState.*;

public class Solution {
    public static void main(String[] args) {
        Dwarf dwarfOne = new Dwarf(100);
        Dwarf dwarfTwo = new Dwarf(100);
        ElfArcher elfArcherOne = new ElfArcher(100);
        ElfArcher elfArcherTwo = new ElfArcher(100);
        ElfSwordsman elfSwordsmanOne = new ElfSwordsman(100);
        ElfSwordsman elfSwordsmanTwo = new ElfSwordsman(100);
        ElfSwordsman elfSwordsmanThree = new ElfSwordsman(100);
        ElfSwordsman elfSwordsmanFour = new ElfSwordsman(100);

        Warrior[] armyForDwarf = {dwarfOne, dwarfTwo, elfSwordsmanOne, elfSwordsmanTwo};
        Warrior[] armyForElf = {elfArcherOne, elfArcherTwo, elfSwordsmanThree, elfSwordsmanFour};

        Solution solution = new Solution();

        solution.battle(armyForDwarf, armyForElf);
        System.out.println();
        solution.print(armyForDwarf);
        solution.print(armyForElf);
    }

    private void print(Warrior[] warriors) {
        System.out.println(Arrays.toString(warriors));
    }

    private void battle(Warrior[] warriors, Warrior[] enemyWarriors) {
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < warriors.length; i++) {
                System.out.println("---start battle---");
                Warrior warrior = warriors[i];
                Warrior enemyWarrior = enemyWarriors[i];
                initWarrior(warrior);
                initWarrior(enemyWarrior);

                System.out.println(warrior);
                System.out.println(enemyWarrior);

                fight(warrior, enemyWarrior);
                fight(enemyWarrior, warrior);

                System.out.println(warrior);
                System.out.println(enemyWarrior);
                System.out.println("---end battle---");
            }
        }
    }

    private void initWarrior(Warrior warrior) {
        warrior.initDefenceLevel();
        warrior.initAttackLevel();
    }

    private void fight(Warrior warriorOne, Warrior warriorTwo) {
        int attackPoints = 0;
        if (warriorOne.getHealthState() != DEAD && warriorOne instanceof Attackable) {
            Attackable one = (Attackable) warriorOne;
            attackPoints = one.attack();
        }
        if (warriorTwo.getHealthState() != DEAD && warriorTwo instanceof Defenceable) {
            Defenceable two = (Defenceable) warriorTwo;
            two.defence(attackPoints);
        }
    }
}
